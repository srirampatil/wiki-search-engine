/**
 * Created by sriram on 12/1/14.
 */

public class SPConstants {
	public static String indexFolderPath = null;
	public static final String STOPWORDS_FILE = "stopwords.txt";
	public static String INDEX_FILE_PATH = null;
	public static String TEMP_INDEX_FILE_PATH = null;
	public static final String DOC_FILE_PATH = "./index/doc_file";
	public static String CHAR_INDEX_FILE_PATH = null;
	public static String TITLE_FILE = null;

	public static final String REMOVAL_REGEX = "[^a-zA-Z=\\[\\]\\{\\}:\\|/ ]";
	public static final String INFOBOX_REGEX = "\\{\\{infobox";
	public static final String CATEGORY_REGEX = "(\\[\\[category:.*?\\]\\])|(==reference.*?==)";
	public static final String HTML_CHARS_REGEX = "(&[^&;]{2,4};)";
	public static final String URL_REGEX = "(http[s]?://)?(www\\.)?.*?\\.[^ ]+";
	public static final String COMMENTS_REGEX = "\\!--.*?--";
	public static final String GR_CURLY_REGEX = "\\{\\{GR\\|[0-9]+\\}\\}";
	public static final String COORD_REGEX = "\\{\\{coord[^\\{]*\\}\\}";
	public static final String CONVERT_REGEX = "\\{\\{convert[^\\{]*\\}\\}";
	public static final String SPACES_REGEX = "( ){2,}";

	public static final char OPENING_SQUARE_BRACE = '[';
	public static final char CLOSING_SQUARE_BRACE = ']';
	public static final char OPENING_CURLY_BRACE = '{';
	public static final char CLOSING_CURLY_BRACE = '}';
	public static final char EQUAL_SIGN = '=';
	public static final char AMPERSAND = '&';
	public static final char HYPHEN = '-';
	public static final char EXCLAMATION = '!';
	public static final char COLON = ':';
	public static final char COMMA = ',';
	public static final char PIPE = '|';
	public static final char DOT = '.';

	public static final String PAGE_TAG = "page";
	public static final String TITLE_TAG = "title";
	public static final String ID_TAG = "id";
	public static final String REVISION_TAG = "revision";
	public static final String TIMESTAMP_TAG = "timestamp";
	public static final String CONTRIBUTOR_TAG = "contributor";
	public static final String USERNAME_TAG = "username";
	public static final String MINOR_TAG = "minor";
	public static final String COMMENT_TAG = "comment";
	public static final String TEXT_TAG = "text";

	public static final String SPECIAL_TAG_GR = "gr";
	public static final String SPECIAL_TAG_INFOBOX = "infobox";
	public static final String SPECIAL_TAG_COORD = "coord";
	public static final String SPECIAL_TAG_CONVERT = "convert";
	public static final String SPECIAL_TAG_CATEGORY = "category";
	public static final String SPECIAL_TAG_SIMPLE = "simple";

	public static final int BLOCK_SIZE = 1;

	public static void setIndexFolderPath(String path) {
		indexFolderPath = path;
		TITLE_FILE = indexFolderPath + "/title.txt";
		INDEX_FILE_PATH = indexFolderPath + "/index.txt";
		TEMP_INDEX_FILE_PATH = indexFolderPath + "/.index_file.txt";
		CHAR_INDEX_FILE_PATH = indexFolderPath + "/char_index.txt";
	}
	
	public static final int ZONE_LINKS = 1;
	public static final int ZONE_CATEGORY = 2;
	public static final int ZONE_BODY = 4;
	public static final int ZONE_INFOBOX = 8;
	public static final int ZONE_TITLE = 64;
	public static final int ZONE_NONE = 0;
	
	public static final int RETRIEVAL_LIMIT = 10;
	
	public static final int DOC_COUNT_LIMIT = 20000;
}
