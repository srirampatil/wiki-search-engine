
public class SPPageData {
	public int totalCount;
	public byte zonePresence;
	public Long docId;
	/*public int[] zoneScoreArray;
	
	private final int[] zoneScores = {1, 3, 5, 2, 10};*/
	
	public SPPageData() {
		totalCount = 0;
		zonePresence = 0;
//		zoneScoreArray = new int[5];
	}
	
	/* public void incrementForType(DataType type) {
		totalCount++;
		
//		int value = type.getValue();
		zonePresence |= (1 << type.getValue());
//		zoneScoreArray[value] += zoneScores[value];
	} */
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(":");
		builder.append((totalCount > 0) ? totalCount : "");
		builder.append(":" + zonePresence);
		return builder.toString();
	}
}
