import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by sriram on 12/1/14.
 */
public class SPPageScanner {
	/**
	 * Different patterns in the text
	 *
	 * // Curly braces
	 * {{Infobox        - infobox   []
	 * {{GR|<num>}}     - ignore    []
	 * {{coord          - ignore (map co-ordinates) []
	 * {{convert        - ignore (unit conversion)  []
	 * {{cite           - ???
	 *
	 * // Square braces
	 * [[category:      - category  []
	 * [[.*?|.*?]]      - wiki links    []
	 * [[simple:.*?]]   - ignore (language translation)     []
	 * [[(.*?){2}:.*?]] - ignore (language translation)     []
	 * [URL words]      - external URLs (ignore URLs take words)
	 *
	 * // Equal sign
	 * references     - references (ignore and scan from next line)
	 * external links - links (ignore and scan from next line)
	 * ==.*?==          - ignore (headers)  []
	 *
	 * // Others
	 * !--.*?--         - comments  []
	 * &[^;]*?{,4};     - HTML chars    []
	 * numbers          - ignore    []
	 * |                - end of clause/statement/word  []
	 */

	private Stemmer stemmer = null;
	public Map<String, SortedMap<Long, SPPageData>> wordsCountMap = null;
	private Long currentDocumentId;

	private String titleString = null;

	public SPPageScanner() {
		super();
		stemmer = new Stemmer();
		wordsCountMap = new TreeMap<String, SortedMap<Long, SPPageData>>();
	}

	public void setDocumetId(Long id) {
		currentDocumentId = id;
		if (titleString != null && currentDocumentId != null) {
			scanText(titleString, SPConstants.ZONE_TITLE);
			titleString = null;
		}
	}

	public void scanTitle(String title) {
		if (currentDocumentId != null) {
			scanText(title, SPConstants.ZONE_TITLE);
		} else
			titleString = title;
	}

	private void addWord(String word, int type) {
		if (SPWikiUtils.stopwordFileExists && SPWikiUtils.isStopword(word))
			return;

		SPPageData data = null;

		stemmer.add(word.toCharArray(), word.length());
		stemmer.stem();
		String stemmedWord = stemmer.toString();

		if (stemmedWord.length() > 0) {
			SortedMap<Long, SPPageData> map = wordsCountMap.get(stemmedWord);
			
			if (wordsCountMap.containsKey(stemmedWord)) {
				if (map.containsKey(currentDocumentId))
					data = map.get(currentDocumentId);
				else
					data = new SPPageData();
				
				data.totalCount++;
				data.zonePresence |= type;
				
				map.put(currentDocumentId, data);
				
			} else {
				map = new TreeMap<Long, SPPageData>();
				data = new SPPageData();
				
				data.totalCount++;
				data.zonePresence |= type;
				
				map.put(currentDocumentId, data);
				wordsCountMap.put(stemmedWord, map);
			}
		}
	}
	
	public void scanText(String text, int type) {

		StringBuilder mutableText = new StringBuilder(text);
		int length = text.length();
		StringBuilder word = new StringBuilder();

		int initialType = type;
		int bracketCount = 0;
		int wLength = 0;
		
		for (int i = 0; i < length; i++) {

			char currentChar = text.charAt(i);
			// Considering only english letters
			boolean isLetter = ((currentChar >= 'a') && (currentChar <= 'z'));

			wLength = 0;
			while (isLetter && i < (length - 1)) {
				word.append(currentChar);
				wLength++;
				currentChar = text.charAt(++i);
				isLetter = ((currentChar >= 'a') && (currentChar <= 'z'));
			}
			
			if(isLetter) {
				word.append(currentChar);
				wLength++;
			}

			if (wLength > 0 && wLength < 100) {    //Word finished
				String w = word.toString();
				if (SPWikiUtils.stopwordFileExists && SPWikiUtils.isStopword(w)) {
					word.setLength(0);
					continue;
				}

				SPPageData data = null;

				stemmer.add(w.toCharArray(), wLength);
				stemmer.stem();
				String stemmedWord = stemmer.toString();

				if (stemmedWord.length() > 0) {
					SortedMap<Long, SPPageData> map = wordsCountMap.get(stemmedWord);
					
					if (wordsCountMap.containsKey(stemmedWord)) {
						if (map.containsKey(currentDocumentId))
							data = map.get(currentDocumentId);
						else
							data = new SPPageData();
						
						data.totalCount += type;
						data.zonePresence |= type;
						
						map.put(currentDocumentId, data);
						
					} else {
						map = new TreeMap<Long, SPPageData>();
						data = new SPPageData();
						
						data.totalCount += type;
						data.zonePresence |= type;
						
						map.put(currentDocumentId, data);
						wordsCountMap.put(stemmedWord, map);
					}
				}
				
				word.setLength(0);
			}

			if ((i + 1) >= length)
				continue;

			String subtag = null;
			char nextChar = text.charAt(i + 1);
			switch (currentChar & nextChar) {
			case SPConstants.OPENING_CURLY_BRACE:
				if ((i + 9) >= length)
					break;

				subtag = mutableText.substring(i + 2, i + 9);
				if (subtag.equals(SPConstants.SPECIAL_TAG_INFOBOX)) {
					type = SPConstants.ZONE_INFOBOX;
					bracketCount++;
					i += 9;
				} else {
					//skip braces
					i += 2;
				}
				break;

			case SPConstants.OPENING_SQUARE_BRACE:
				if ((i + 9) < length) {      //For category
					subtag = text.substring(i + 2, i + 10);
					if (subtag.equals(SPConstants.SPECIAL_TAG_CATEGORY)) {
						type = SPConstants.ZONE_CATEGORY;
						bracketCount++;
						i += 10;

					} else
						i += 2;
				}
				break;

			case SPConstants.CLOSING_CURLY_BRACE:
				if (bracketCount > 0)
					bracketCount--;

				i += 2;
				break;

			case SPConstants.CLOSING_SQUARE_BRACE:
				if (bracketCount > 0)
					bracketCount--;

				i += 2;
				break;

			default:
				if (currentChar == SPConstants.OPENING_SQUARE_BRACE
						&& nextChar != SPConstants.OPENING_SQUARE_BRACE) {

					type = SPConstants.ZONE_LINKS;
					bracketCount++;

				} else if (currentChar == SPConstants.CLOSING_SQUARE_BRACE
						&& nextChar != SPConstants.CLOSING_SQUARE_BRACE) {

					if (bracketCount > 0)
						bracketCount--;
				}
			}

			if (type != initialType && bracketCount == 0)
				type = initialType;
		}

		if (word.length() > 0) {
			String w = word.toString();
			if (SPWikiUtils.stopwordFileExists && SPWikiUtils.isStopword(w))
				return;

			SPPageData data = null;

			stemmer.add(w.toCharArray(), wLength);
			stemmer.stem();
			String stemmedWord = stemmer.toString();

			if (stemmedWord.length() > 0) {
				SortedMap<Long, SPPageData> map = wordsCountMap.get(stemmedWord);
				
				if (wordsCountMap.containsKey(stemmedWord)) {
					if (map.containsKey(currentDocumentId))
						data = map.get(currentDocumentId);
					else
						data = new SPPageData();
					
					data.totalCount++;
					data.zonePresence |= type;
					
					map.put(currentDocumentId, data);
					
				} else {
					map = new TreeMap<Long, SPPageData>();
					data = new SPPageData();
					
					data.totalCount++;
					data.zonePresence |= type;
					
					map.put(currentDocumentId, data);
					wordsCountMap.put(stemmedWord, map);
				}
			}
		}
	}

	public void printKeywords() {
		System.out.println(wordsCountMap.size());
		/* for(Iterator<String> it = wordsCountMap.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			System.out.println(key + ": " + wordsCountMap.get(key));
		} */
	}
}
