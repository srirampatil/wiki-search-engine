import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by sriram on 14/1/14.
 */
public class SPWikiIndexFileReader {

	private Stemmer stemmer;
	private Map<String, String> keyOffsetMap = null;
	private Map<String, String> docTitlesMap = null;

	public SPWikiIndexFileReader() throws IOException {
		super();
		stemmer = new Stemmer();

		keyOffsetMap = new HashMap<String, String>();
		docTitlesMap = new HashMap<String, String>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(
					SPConstants.CHAR_INDEX_FILE_PATH));
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] splits = line.split(" ");
					if (splits.length == 2)
						keyOffsetMap.put(splits[0], splits[1]);
				}

				reader.close();
				reader = new BufferedReader(new FileReader(
						SPConstants.TITLE_FILE));
				while ((line = reader.readLine()) != null) {
					String[] splits = line.split(":");
					if (splits.length == 2)
						docTitlesMap.put(splits[0], splits[1]);
				}
			} catch (IOException e) {
				System.err.println(SPConstants.TITLE_FILE
						+ ": Error opening file.");
				throw e;
			} finally {
				reader.close();
			}

		} catch (IOException e) {
			System.err.println(SPConstants.CHAR_INDEX_FILE_PATH
					+ ": Error opening file.");
			throw e;
		}
	}

	private String processString(String word) {
		String lowercaseWord = word.toLowerCase();
		if (SPWikiUtils.stopwordFileExists
				&& SPWikiUtils.isStopword(lowercaseWord))
			return null;

		stemmer.add(lowercaseWord.toCharArray(), lowercaseWord.length());
		stemmer.stem();
		return stemmer.toString();
	}

	private Set<Long> getDocIds(String postingsList, int length, int zone) {
		Set<Long> docSet = new LinkedHashSet<Long>();

		boolean colonFound = false, isNegative = false;
		long number = 0, docId = 0;
		for (int i = 0; i < length; i++) {
			switch (postingsList.charAt(i)) {
			case SPConstants.COLON:
				colonFound = true;
				docId = number;
				number = 0;
				break;

			case SPConstants.COMMA:
				// AND condition (> 0 for OR)
				if (colonFound) {
					number = (isNegative) ? -number : number;
					if (zone == SPConstants.ZONE_NONE)
						docSet.add(docId);
					else if ((number & zone) == zone)
						docSet.add(docId);
				}

				colonFound = false;
				number = 0;
				isNegative = false;
				break;

			case SPConstants.PIPE:
			case SPConstants.DOT:
				number = 0;
				isNegative = false;
				break;

			case SPConstants.HYPHEN:
				isNegative = true;
				break;
				
			default:
				number = number * 10
						+ Character.getNumericValue(postingsList.charAt(i));
			}
		}

		return docSet;
	}

	private Set<Long> searchIndex(String word, long startOffset, int zone)
			throws IOException {

		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(
					SPConstants.INDEX_FILE_PATH));
			reader.skip(startOffset);

			String line = null;
			//while ((line = reader.readLine()) != null) {
			line = reader.readLine();
			int length = line.length();

			int hyphenIndex = 0;
			for (; hyphenIndex < length
					&& line.charAt(hyphenIndex) != '-'; hyphenIndex++)
				;
			if (hyphenIndex == length) {
				System.err.println("Hyphen not found!");
				return null;
			}

			String keyword = line.substring(0, hyphenIndex);
			String postingsList = line.substring(hyphenIndex + 1);

			int result = word.compareTo(keyword);
			if (result == 0) {
				reader.close();
				Set<Long> docSet = getDocIds(postingsList, length - hyphenIndex
						- 1, zone);
				return docSet;

			} else if (result < 0) {
				return null;
			}
			//}
		} catch (IOException e) {
			System.err.println(SPConstants.INDEX_FILE_PATH
					+ ": Error opening file.");
			throw e;
		} finally {
			reader.close();
		}

		return null;
	}

	public Map<Long, String> parseQuery(String query) {
		int length = query.length();

		Map<String, Integer> keywordZoneMap = new HashMap<String, Integer>();

		StringBuilder token = new StringBuilder();
		int zone = SPConstants.ZONE_NONE;

		for (int i = 0; i < length; i++) {
			switch (query.charAt(i)) {
			case SPConstants.COLON:
				if (token.length() > 1) {
					System.err.println("Unknown field");
					token.setLength(0);
					continue;
				}

				if(token.length() == 0)
					continue;
				
				switch (token.charAt(0)) {
				case 't':
					zone |= SPConstants.ZONE_TITLE;
					break;

				case 'i':
					zone |= SPConstants.ZONE_INFOBOX;
					break;

				case 'l':
					zone |= SPConstants.ZONE_LINKS;
					break;

				case 'b':
					zone |= SPConstants.ZONE_BODY;
					break;

				case 'c':
					zone |= SPConstants.ZONE_CATEGORY;
					break;
				}

				token.setLength(0);
				break;

			case ' ':
				Integer oldZone = keywordZoneMap.get(token);
				if (oldZone != null) {
					oldZone |= zone;
					keywordZoneMap.put(token.toString(), oldZone);
				} else
					keywordZoneMap.put(token.toString(), zone);

				token.setLength(0);
				zone = SPConstants.ZONE_NONE;

				break;

			default:
				token.append(query.charAt(i));
			}
		}

		if (token.length() > 0) {
			Integer oldZone = keywordZoneMap.get(token);
			if (oldZone != null) {
				oldZone |= zone;
				keywordZoneMap.put(token.toString(), oldZone);
			} else
				keywordZoneMap.put(token.toString(), zone);
		}

		Set<Long> docSet = null;

		for (String keyword : keywordZoneMap.keySet()) {
			try {
				if (docSet == null)
					docSet = searchSingleWord(keyword,
							keywordZoneMap.get(keyword));
				else {
					Set<Long> nextSet = searchSingleWord(keyword,
							keywordZoneMap.get(keyword));
					if(nextSet != null)
						docSet.retainAll(nextSet);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if(docSet == null)
			return null;
		
		Map<Long, String> titlesMap = new LinkedHashMap<Long, String>();
		int docCount = 0;
		for(Long docId : docSet) {
			titlesMap.put(docId, docTitlesMap.get(String.valueOf(docId)));
			if(++docCount == SPConstants.RETRIEVAL_LIMIT)
				break;
		}
		
		return titlesMap;
	}

	private Set<Long> searchSingleWord(String word, int zone)
			throws IOException {
		if (word == null || word.length() == 0)
			return null;

		String processedWord = processString(word);
		if (processedWord == null || processedWord.length() == 0)
			return null;

		//long time = System.currentTimeMillis();
		/* int index = Collections.binarySearch(keyList, processedWord);
		index = (index < 0) ? -index - 2 : index;

		if (index > 0 && index == offsetList.size() - 1)
			index--; */

		String offsetString = keyOffsetMap.get(processedWord);
		if(offsetString == null)
			return null;
		
		return searchIndex(processedWord, Long.parseLong(offsetString), zone);
	}
}
