import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by sriram on 13/1/14.
 */
public class SPWikiIndexFileWriter {

	private File indexFile;
	private ExecutorService executor;

	public SPWikiIndexFileWriter() {
		super();

		executor = new ThreadPoolExecutor(1, 1, 6L, TimeUnit.HOURS,
				new LinkedBlockingQueue<Runnable>(),
				new RejectedExecutionHandler() {
					@Override
					public void rejectedExecution(Runnable runnable,
							ThreadPoolExecutor threadPoolExecutor) {
						System.out.println("Rejection happened");
					}
				});

		indexFile = new File(SPConstants.INDEX_FILE_PATH);
		if (indexFile.exists())
			indexFile.delete();
	}

	private AtomicInteger tempFileCount = new AtomicInteger(1);
	private final int TEMP_FILES_MAX_COUNT = 1500;

	private void mergeFiles() {
		lock.lock();

		File indexFile = new File(SPConstants.INDEX_FILE_PATH);
		long time = System.currentTimeMillis();

		List<BufferedReader> readersList = new ArrayList<BufferedReader>(
				TEMP_FILES_MAX_COUNT);

		if (new File(SPConstants.INDEX_FILE_PATH).exists()) {
			try {
				readersList.add(new BufferedReader(new FileReader(
						SPConstants.INDEX_FILE_PATH)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		int currentCount = tempFileCount.get();
		for (int i = 1; i < currentCount; i++) {
			try {
				readersList.add(new BufferedReader(new FileReader(
						SPConstants.indexFolderPath + "/index_" + i)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		BufferedWriter writer = null;
		BufferedWriter charIndexWriter = null;
		try {
			writer = new BufferedWriter(new FileWriter(
					SPConstants.TEMP_INDEX_FILE_PATH));

			charIndexWriter = new BufferedWriter(new FileWriter(
					SPConstants.CHAR_INDEX_FILE_PATH));

			PriorityQueue<String> priorityQ = new PriorityQueue<String>();
			int size = readersList.size();
			Map<String, String> map = new HashMap<String, String>();
			Map<String, Integer> indexMap = new HashMap<String, Integer>();

			int index = 0;

			String line = null, key = null, postingsList = null;

			for (; index < size && priorityQ.size() < TEMP_FILES_MAX_COUNT; index++) {
				BufferedReader reader = readersList.get(index);

				while (true) {
					line = reader.readLine();
					if (line == null) {
						size--;
						break;
					}

					int hyphenIndex = 0;
					for (int length = line.length(); hyphenIndex < length
							&& line.charAt(hyphenIndex) != '-'; hyphenIndex++)
						;
					key = line.substring(0, hyphenIndex);
					postingsList = line.substring(hyphenIndex + 1);

					if (!priorityQ.contains(key)) {
						priorityQ.offer(key);
						map.put(key, line);
						indexMap.put(key, index);
						break;
					}

					map.put(key, map.get(key) + postingsList);
				}
			}

			long linesWritten = 0;
			while (size > 1) {
				if (priorityQ.isEmpty())
					continue;

				key = priorityQ.peek();

				rewriteWithTfIdf(writer, charIndexWriter, map.get(key) + "\n");

				map.remove(priorityQ.poll());

				int currentIndex = indexMap.get(key);
				indexMap.remove(key);
				BufferedReader reader = readersList.get(currentIndex);

				while (true) {
					line = reader.readLine();
					if (line == null) {
						size--;
						break;
					}

					int hyphenIndex = 0;
					for (int length = line.length(); hyphenIndex < length
							&& line.charAt(hyphenIndex) != '-'; hyphenIndex++)
						;
					key = line.substring(0, hyphenIndex);
					postingsList = line.substring(hyphenIndex + 1);

					if (!priorityQ.contains(key)) {
						priorityQ.offer(key);
						map.put(key, line);
						indexMap.put(key, currentIndex);
						break;
					}

					// Maintain the doc ids in sorted order
					if (currentIndex < indexMap.get(key)) {
						String oldKeyPostings = map.get(key);
						int oldHyphenIndex = 0;
						for (int length = oldKeyPostings.length(); oldHyphenIndex < length
								&& oldKeyPostings.charAt(oldHyphenIndex) != '-'; oldHyphenIndex++)
							;

						map.put(key,
								oldKeyPostings.substring(0, oldHyphenIndex)
										+ "-"
										+ postingsList
										+ oldKeyPostings
												.substring(oldHyphenIndex + 1));
					} else
						map.put(key, map.get(key) + postingsList);
				}

				linesWritten++;
			}

			while (!priorityQ.isEmpty()) {
				rewriteWithTfIdf(writer, charIndexWriter,
						map.get(priorityQ.peek()) + "\n");

				map.remove(priorityQ.poll());
				linesWritten++;
			}

			if (size == 1) {
				BufferedReader reader = readersList.get(0);
				while ((line = reader.readLine()) != null) {
					rewriteWithTfIdf(writer, charIndexWriter, line + "\n");
					linesWritten++;
				}
			}

			System.out.println("Lines Written: " + linesWritten);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				charIndexWriter.write("& " + offset + "\n");

				charIndexWriter.flush();
				charIndexWriter.close();

				writer.flush();
				writer.close();

				for (BufferedReader reader : readersList)
					reader.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

			if (indexFile.exists())
				indexFile.delete();

			File tempIndexFile = new File(SPConstants.TEMP_INDEX_FILE_PATH);
			tempIndexFile.renameTo(indexFile);

			tempFileCount.set(1);

			System.out.println("Merge time: "
					+ (System.currentTimeMillis() - time) / 1000f);

			cond.signalAll();
			lock.unlock();
		}
	}

	private Lock lock = new ReentrantLock();
	private Condition cond = lock.newCondition();

	public void add(final Map<String, SortedMap<Long, SPPageData>> keywordsMap) {
		lock.lock();
		while (tempFileCount.get() > TEMP_FILES_MAX_COUNT) {
			try {
				cond.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		executor.execute(new Runnable() {
			@Override
			public void run() {
				//Writing the file

				int currentCount = tempFileCount.getAndIncrement();
				if (currentCount == TEMP_FILES_MAX_COUNT) {
					mergeFiles();
					currentCount = tempFileCount.getAndIncrement();
				}

				String fileName = new String(SPConstants.indexFolderPath
						+ "/index_" + currentCount);

				System.out.println("Writing file " + fileName);

				BufferedWriter bw = null;
				try {
					bw = new BufferedWriter(new FileWriter(fileName));
				} catch (IOException e) {
					e.printStackTrace();
				}

				StringBuilder builder = new StringBuilder();
				for (String keyword : keywordsMap.keySet()) {
					SortedMap<Long, SPPageData> dataMap = keywordsMap
							.get(keyword);

					builder.append(keyword + "-");
					int docCount = 0;
					for (Long docId : dataMap.keySet()) {
						SPPageData data = dataMap.get(docId);
						builder.append(docId + ":" + data.totalCount + ":"
								+ data.zonePresence + ",");

						if (++docCount == SPConstants.DOC_COUNT_LIMIT)
							break;
					}

					builder.append("\n");
					try {
						bw.write(builder.toString());
					} catch (IOException e) {
						e.printStackTrace();
					}
					builder.setLength(0);
				}

				try {
					bw.flush();
					bw.close();

					System.out.println("Stored file " + fileName);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		lock.unlock();
	}

	public final int DOC_INDEX = 0;
	public final int TF_INDEX = 1;
	public final int ZONE_INDEX = 2;

	long offset = 0;
	int linesCount = 0;

	public void rewriteWithTfIdf(BufferedWriter writer,
			BufferedWriter charIndexWriter, String line) {

		try {
			int hyphenIndex = 0;
			for (int length = line.length(); hyphenIndex < length
					&& line.charAt(hyphenIndex) != '-'; hyphenIndex++)
				;

			String keyword = line.substring(0, hyphenIndex);
			String postingsList = line.substring(hyphenIndex + 1);

			if (linesCount == SPConstants.BLOCK_SIZE) {
				charIndexWriter.write(keyword + " " + offset + "\n");
				linesCount = 0;
			}

			if (offset == 0)
				charIndexWriter.write(keyword + " " + offset + "\n");

			String[] documents = postingsList.split(",");
			long df = documents.length;
			double idf = Double.parseDouble(String.format("%.2f",
					Math.log10(SPWikiXMLParser.totalDocuments / df)));

			SortedMap<Double, List<String>> tfidfMap = new TreeMap<Double, List<String>>(
					new Comparator<Double>() {

						@Override
						public int compare(Double o1, Double o2) {
							return o2.compareTo(o1);
						}
					});

			int docCount = 0;
			for (int i = 0; i < df && docCount < SPConstants.DOC_COUNT_LIMIT; i++) {
				String[] docData = documents[i].split(":");

				if (docData.length < 3)
					continue;

				double tfidf = Integer.parseInt(docData[TF_INDEX]) * idf;

				List<String> docList = tfidfMap.get(tfidf);
				if (docList == null)
					docList = new ArrayList<String>();

				docList.add(docData[DOC_INDEX] + ":" + docData[ZONE_INDEX]);
				tfidfMap.put(tfidf, docList);
				docCount++;
			}

			StringBuilder newLine = new StringBuilder(keyword + "-");
			for (Double tfidf : tfidfMap.keySet()) {
				String tfidfStr = String.valueOf(tfidf);
				newLine.append(tfidfStr + ",");

				offset += (tfidfStr.length() + 1);

				for (String docId : tfidfMap.get(tfidf)) {
					newLine.append(docId + ",");
					offset += (docId.length() + 1);
				}

				newLine.append("|");
				offset += 1;
			}

			newLine.append("\n");
			writer.write(newLine.toString());

			offset += (keyword.length() + 1 + 1);
			linesCount++;

			//}

		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void commitOffsets() {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("Last merge");
				mergeFiles();
			}
		});

		lock.lock();
		while (tempFileCount.get() != 1) {
			try {
				cond.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		lock.unlock();

		executor.shutdown();
		try {
			executor.awaitTermination(60L, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
