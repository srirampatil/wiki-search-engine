import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

/**
 * Created by sriram on 7/1/14.
 */
public class SPWikiIndexingMain {

	private static void printUsage() {
		System.out.println("java SPWikiIndexingMain <path to sample.xml> <path to index folder>");
	}

    public static void main(String[] args) {
		if(args.length != 2) {
			System.err.println("Invalid number of input arguments.");
			printUsage();
			return;
		}

		File inputFile = new File(args[0]);
		if(!inputFile.exists()) {
			System.err.println(args[0] + " does not exist.");
			return;
		}

		File indexFolder = new File(args[1]);
		if(!indexFolder.exists()) {
			if(!indexFolder.mkdir()) {
				System.err.println("Unable to created " + args[1] + " directory.");
				return;
			}
		}

		SPConstants.setIndexFolderPath(args[1]);

		long time = System.currentTimeMillis();
		final String inputFilePath = args[0];
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				SPWikiXMLParser parser = new SPWikiXMLParser();

		        try {
		            parser.parse(inputFilePath);
		        } catch (IOException e) {
		            System.out.println(e.getLocalizedMessage());
		        } catch (SAXException e) { 
		            System.out.println(e.getLocalizedMessage());
		        }
			}
		});
        
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println((System.currentTimeMillis() - time) / 1000f);
	}
}
