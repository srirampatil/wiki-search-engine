import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Created by sriram on 14/1/14.
 */
public class SPWikiSearchMain {
	private static void printUsage() {
		System.out.println("java SPWikiSearchMain <path to index folder>");
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Invalid number of input arguments.");
			printUsage();
			return;
		}

		File indexFolder = new File(args[0] + "/index.txt");
		if (!indexFolder.exists()) {
			System.err.println(args[0] + "/index.txt does not exist.");
			return;
		}

		SPConstants.setIndexFolderPath(args[0]);
		SPWikiIndexFileReader reader = null;
		try {
			reader = new SPWikiIndexFileReader();
		} catch (IOException e) {
			return;
		}

		Scanner sc = new Scanner(System.in);
		int count = 0;

		try {
			count = sc.nextInt();
		} catch (Exception e) {
			System.err.println("Invalid input, expected an integer");
		}

		while (count-- > 0) {
			String word = null;
			try {
				word = sc.nextLine();
			} catch (NoSuchElementException e) {
				break;
			}

			if(word.length() == 0)
				continue;
			
			long time = System.currentTimeMillis();
			Map<Long, String> titlesMap = reader.parseQuery(word);
			if(titlesMap == null)
				continue;
			
			for(Long docId : titlesMap.keySet())
				System.out.println(docId + " " + titlesMap.get(docId));
			
			System.out.println("Time: " + (System.currentTimeMillis() - time)
					/ 1000f + " seconds");
		}
	}
}
