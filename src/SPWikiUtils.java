import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sriram on 14/1/14.
 */
public class SPWikiUtils {
	private static Set<String> stopwordsSet = null;
	public static boolean stopwordFileExists = true;

	static {
		File f = new File(SPConstants.STOPWORDS_FILE);
		if (!f.exists()) {
			stopwordFileExists = false;
			System.err.println("Stopwords file does not exist.");
		}
	}

	private static void readStopwords() {
		if (stopwordsSet != null)
			return;

		stopwordsSet = new HashSet<String>();

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(SPConstants.STOPWORDS_FILE));
			String line = null;
			while ((line = reader.readLine()) != null)
				stopwordsSet.add(line);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isStopword(String word) {
		if (!stopwordFileExists)
			return false;
		
		if(stopwordsSet != null)
			return stopwordsSet.contains(word);
		else {
			readStopwords();
			if (stopwordsSet == null)
				return false;
		}

		return stopwordsSet.contains(word);
	}
}
