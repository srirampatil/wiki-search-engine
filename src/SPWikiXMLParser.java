import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by sriram on 12/1/14.
 */
public class SPWikiXMLParser {
	private SAXParser parser;
	private Long currentDocumentId = null;
	private String docTitle = null;

	public static long totalDocuments = 0;
	private BufferedWriter titleWriter = null;

	private class SPWikiParserHandler extends DefaultHandler {
		private Stack<String> tagsStack;		// Stack to hold the XML tags
		private StringBuilder tagContents;
		private SPPageScanner pageScanner = null;
		private SPWikiIndexFileWriter indexer = new SPWikiIndexFileWriter();

		private final int BUFF_SIZE = 15000;
		private int buffSize = 0;

		public SPWikiParserHandler() {
			super();
			tagsStack = new Stack<String>();
			tagContents = new StringBuilder();
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			tagsStack.push(qName.toLowerCase());
			tagContents.setLength(0);

			if (qName.equalsIgnoreCase(SPConstants.PAGE_TAG)) {
				totalDocuments++;
				//System.out.println(totalDocuments);

				if (buffSize == BUFF_SIZE)
					indexer.add(pageScanner.wordsCountMap);

				if (buffSize == BUFF_SIZE || buffSize == 0) {
					pageScanner = new SPPageScanner();
					buffSize = 0;
				}
			}
		}

		@Override
		public void characters(char[] chars, int start, int length)
				throws SAXException {
			tagContents.append(chars, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			String tag = tagsStack.peek();
			String lowerCaseQName = qName.toLowerCase();
			if (!lowerCaseQName.equals(tag))
				throw new InternalError(
						"The start and end tags did not match: " + qName);

			tagsStack.pop();
			if (tagsStack.isEmpty())
				return;

			// Case folding
			String lowerCaseTagContents = String.valueOf(tagContents)
					.toLowerCase();
			String parentTag = tagsStack.peek();

			if (parentTag.equals(SPConstants.PAGE_TAG)) {
				if (lowerCaseQName.equals(SPConstants.ID_TAG)) {
					currentDocumentId = Long.parseLong(String
							.valueOf(lowerCaseTagContents));

					if (docTitle != null) {
						try {
							titleWriter.write(currentDocumentId
									+ ":" + docTitle + "\n");
						} catch (IOException e) {
							e.printStackTrace();
						}

						docTitle = null;
					}

					pageScanner.setDocumetId(currentDocumentId);

				} else if (lowerCaseQName.equals(SPConstants.TITLE_TAG)) {
					if (currentDocumentId != null) {
						try {
							titleWriter.write(currentDocumentId
									+ ":" + tagContents.toString() + "\n");
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else
						docTitle = tagContents.toString();

					pageScanner.scanTitle(String.valueOf(lowerCaseTagContents));
				}

			} else if (parentTag.equals(SPConstants.REVISION_TAG)) {
				if (lowerCaseQName.equals(SPConstants.TEXT_TAG))
					pageScanner.scanText(String.valueOf(lowerCaseTagContents),
							SPConstants.ZONE_BODY);

			} else if (parentTag.equals(SPConstants.CONTRIBUTOR_TAG)) {
				if (lowerCaseQName.equals(SPConstants.ID_TAG))
					Long.parseLong(String.valueOf(lowerCaseTagContents));

				else if (lowerCaseQName.equals(SPConstants.USERNAME_TAG))
					String.valueOf(lowerCaseTagContents);

			} else if (qName.equals(SPConstants.PAGE_TAG)) {
				currentDocumentId = null;
				docTitle = null;
				pageScanner.setDocumetId(null);
				buffSize++;
			}
		}

		@Override
		public void endDocument() throws SAXException {
			try {
				titleWriter.flush();
				titleWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			indexer.add(pageScanner.wordsCountMap);
			indexer.commitOffsets();
			buffSize = 0;

			System.out.println("Total Documents: " + totalDocuments);
		}
	}

	public SPWikiXMLParser() {
		super();

		try {
			titleWriter = new BufferedWriter(new FileWriter(
					SPConstants.TITLE_FILE));
			this.parser = SAXParserFactory.newInstance().newSAXParser();
		} catch (ParserConfigurationException e) {
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		} catch (SAXException e) {
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void parse(String filePath) throws IOException, SAXException {
		this.parser.parse(filePath, new SPWikiParserHandler());
		//pageScanner.printKeywords();
	}
}
